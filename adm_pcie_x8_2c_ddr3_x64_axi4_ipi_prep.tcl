#
# Name: adm_pcie_x8_2c_ddr3_x64_axi4_ipi_prep.tcl
#
# Description: This script prepares relevant IP cores for instantiations in the
#  Vivado project that houses the design.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

# PROPRIETARY IP REPOSITORY PATH
#
set ip_repo "/vol/designs/chdhung/Vivado_IP"

# COMMON VARIABLES
#
set proj_dir [file normalize [get_property DIRECTORY [current_project]]]
set proj_name [get_property NAME [current_project]]
set fset_name [get_property NAME [current_fileset]]

# ADD PROPRIETARY IP REPOSITORY TO VIVADO
#
set orig_ip_repo [get_property ip_repo_paths [current_project]]

if {[lsearch $orig_ip_repo $ip_repo] < 0} {
    set_property ip_repo_paths [list $orig_ip_repo $ip_repo] [current_project]

    update_ip_catalog -rebuild
}

# CUSTOMISE VIRTUAL INPUT/OUTPUT CONTROL PANEL
#
create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_master
set_property -dict [list CONFIG.C_PROBE_OUT0_INIT_VAL {0x0} CONFIG.C_PROBE_OUT1_INIT_VAL {0x1} CONFIG.C_PROBE_OUT2_INIT_VAL {0x0} CONFIG.C_PROBE_OUT3_INIT_VAL {0x0} CONFIG.C_PROBE_OUT4_INIT_VAL {0x0} CONFIG.C_PROBE_IN8_WIDTH {64} CONFIG.C_PROBE_IN7_WIDTH {64} CONFIG.C_PROBE_IN6_WIDTH {2} CONFIG.C_PROBE_IN5_WIDTH {2} CONFIG.C_NUM_PROBE_IN {9} CONFIG.C_NUM_PROBE_OUT {5}] [get_ips vio_master]

create_ip -name ila -vendor xilinx.com -library ip -version 6.0 -module_name ila_axi4_rd
set_property -dict [list CONFIG.C_PROBE15_TYPE {1} CONFIG.C_PROBE14_TYPE {1} CONFIG.C_PROBE13_TYPE {1} CONFIG.C_PROBE12_TYPE {1} CONFIG.C_PROBE9_TYPE {1} CONFIG.C_PROBE8_TYPE {1} CONFIG.C_PROBE7_TYPE {1} CONFIG.C_PROBE6_TYPE {1} CONFIG.C_PROBE5_TYPE {1} CONFIG.C_PROBE4_TYPE {1} CONFIG.C_PROBE3_TYPE {1} CONFIG.C_PROBE2_TYPE {1} CONFIG.C_PROBE1_TYPE {1} CONFIG.C_PROBE14_WIDTH {2} CONFIG.C_PROBE13_WIDTH {512} CONFIG.C_PROBE12_WIDTH {4} CONFIG.C_PROBE11_WIDTH {1} CONFIG.C_PROBE9_WIDTH {4} CONFIG.C_PROBE8_WIDTH {3} CONFIG.C_PROBE7_WIDTH {4} CONFIG.C_PROBE5_WIDTH {2} CONFIG.C_PROBE4_WIDTH {3} CONFIG.C_PROBE3_WIDTH {8} CONFIG.C_PROBE2_WIDTH {33} CONFIG.C_PROBE1_WIDTH {4} CONFIG.C_NUM_OF_PROBES {18}] [get_ips ila_axi4_rd]

create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_axi4_rd
set_property -dict [list CONFIG.C_NUM_PROBE_IN {8} CONFIG.C_NUM_PROBE_OUT {11} CONFIG.C_PROBE_IN1_WIDTH {4} CONFIG.C_PROBE_IN2_WIDTH {256} CONFIG.C_PROBE_IN3_WIDTH {256} CONFIG.C_PROBE_IN4_WIDTH {2} CONFIG.C_PROBE_IN5_WIDTH {1} CONFIG.C_PROBE_OUT0_WIDTH {4} CONFIG.C_PROBE_OUT1_WIDTH {33} CONFIG.C_PROBE_OUT2_WIDTH {8} CONFIG.C_PROBE_OUT3_WIDTH {3} CONFIG.C_PROBE_OUT4_WIDTH {2} CONFIG.C_PROBE_OUT6_WIDTH {4} CONFIG.C_PROBE_OUT7_WIDTH {3} CONFIG.C_PROBE_OUT8_WIDTH {4}] [get_ips vio_axi4_rd]

# CONFIGURE IMPLEMENTATION STRATEGY FOR VIVADO TO TRY HARDER WITH PERFORMANCE-
#  BOUNDED DESIGN
#
set_property strategy Performance_ExplorePostRoutePhysOpt [get_runs impl_1]

# CONSTRAIN INTERFACE PORTS OF ALPHA-DATA ADM-PCIE-7V3 PCIE TARGET BRIDGE AND
#  MEMORY INTERFACE GENERATOR WRAPPER TO PHYSICAL PINS
#
set_property SCOPED_TO_REF adm_pcie_x8_2c_ddr3_x64_axi4_ipi [get_files adm_pcie_x8.xdc]
set_property SCOPED_TO_REF adm_pcie_x8_2c_ddr3_x64_axi4_ipi [get_files adm_2c_ddr3_x64.xdc]
# set_property PROCESSING_ORDER LATE [get_files adm_pcie_x8.xdc]
# set_property PROCESSING_ORDER LATE [get_files adm_2c_ddr3_x64.xdc]