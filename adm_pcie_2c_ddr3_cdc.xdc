#
# adm_pcie_2c_ddr3_cdc.xdc
#
# Short Description: The Xilinx Design Constraints file that disables certain
#  nets of asynchronous nature from timing analysis.
#
# Long Description: The Xilinx Design Constraints file that disables certain
#  nets of asynchronous nature from timing analysis.
#
# Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

# FROM PCIE DOMAIN (I) TO USER CLOCK DOMAIN
#
set_false_path -from [get_clocks I] -to [get_clocks userclk2]
set_false_path -from [get_clocks userclk2] -to [get_clocks I]

# FROM PCIE DOMAIN (I) TO DDR3 DOMAIN
#
set_false_path -from [get_clocks I] -to [get_clocks clk_pll_i*]
set_false_path -from [get_clocks clk_pll_i*] -to [get_clocks I]

# FROM COMMON REFERENCE CLOCK DOMAIN TO DDR3 (C0/C1) DOMAIN (clk_pll_i/clk_pll_i_1)
#
set_false_path -from [get_clocks rclkin] -to [get_clocks clk_pll_i*]
set_false_path -from [get_clocks clk_pll_i*] -to [get_clocks rclkin]

# FROM PCIE RESET OUTPUT TO ALL INTERMEDIATE AXI RESETS AND DDR3 SYSTEM RESET
#
set_false_path -from [get_pins -hier -filter {NAME=~*adb3_admpcie7v3_x8_axi4_ipi_0/U0/gen_synth.srst_n_reg/C}]