#
# adm_2c_ddr3_x64.xdc
#
# Short Description: The Xilinx Design Constraints files that connects FPGA pins
#  with the Memory Interface encapsulation module and creates related clocks for
#  synthesis.
#
# Long Description: The Xilinx Design Constraints files that connects FPGA pins
#  with the Memory Interface encapsulation module and creates related clocks for
#  synthesis.
#
# Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

# MEMORY CONTROLLER SYSTEM CLOCK
#
#  AS ILLUSTRATED IN ADM-PCIE-7V3 USER MANUAL V1.4
#
# 400MHZ CLOCK SOURCE
#
#  AS POINTED OUT IN FIGURE 2: BLOCK DIAGRAM OF STANDALONE DIMM TEST FPGA DESIGN
#
set_property IOSTANDARD DIFF_SSTL15 [get_ports c?_sys_clk_?]
set_property PACKAGE_PIN AH15 [get_ports c0_sys_clk_p]
set_property PACKAGE_PIN AJ15 [get_ports c0_sys_clk_n]
set_property PACKAGE_PIN G30 [get_ports c1_sys_clk_p]
set_property PACKAGE_PIN G31 [get_ports c1_sys_clk_n]

# IODELAYCTRL REFERENCE CLOCK
#
#  AS ILLUSTRATED IN ADM-PCIE-7V3 USER MANUAL V1.4
#
# 200 MHZ CLOCK SOURCE
#
#  AS POINTED OUT IN FIGURE 2: BLOCK DIAGRAM OF STANDALONE DIMM TEST FPGA DESIGN
#
set_property IOSTANDARD DIFF_HSTL_I [get_ports clk_ref_?]
set_property PACKAGE_PIN AD29 [get_ports clk_ref_p]
set_property PACKAGE_PIN AE29 [get_ports clk_ref_n]

# HIGH-PERFORMANCE INTERFACE POWER-SAVING DESIGN (DDR3 SUBSYSTEM)
#
#  AS ILLUSTRATED IN SECTION 2.2.3 OF ADM-PCIE-7V3 USER MANUAL V1.4
#   AND FIGURE 2: BLOCK DIAGRAM OF STANDALONE DIMM TEST FPGA DESIGN
#
#  IOSTANDARD QUOTED FROM ADM-PCIE-7V3 PINOUT TABLE THAT HAS MORE ACCURATE DATA
#   ON BANK VOLTAGE
#
set_property IOSTANDARD LVCMOS18 [get_ports dram_pwr_*]
set_property PACKAGE_PIN AA24 [get_ports dram_pwr_on]
set_property PACKAGE_PIN AA31 [get_ports dram_pwr_ok]

# DDR3 INTERFACE
#
# ALL PINS ARE "STUB SERIES TERMINATED LOGIC" STANDARD DESIGNED SPECIFICALLY FOR
#  DDR SYSTEM
#
# 1) ASSIGN ALL PINS AS SSTL 1.5V IOSTANDARD
# 2) OVERRIDE CLOCK PIN TO USE DIFFERENTIAL SSTL 1.5V IOSTANDARD
#     AND OVERRIDE DATA PINS TO USE DIFFERENTIAL SSTL 1.5V TRI-STATE DIGITALLY
#          CONTROLLED IMPEDANCE IOSTANDARD
#     AND OVERRIDE DATA STROBE PINS TO USE BI-DIRECTIONAL DIFFERENTIAL SSTL 1.5V
#          TRI-STATE DIGITALLY CONTROLLED IMPEDANCE IOSTANDARD
#
#  DERIVED FROM CONSTRAINTS FILES OF ALPHA-DATA DIMM STANDALONE TEST V2.0
#
set_property IOSTANDARD SSTL15 [get_ports c?_ddr3_*]
set_property IOSTANDARD DIFF_SSTL15 [get_ports c?_ddr3_ck_*]
set_property IOSTANDARD SSTL15_T_DCI [get_ports c?_ddr3_dq*]
set_property IOSTANDARD DIFF_SSTL15_T_DCI [get_ports c?_ddr3_dqs_*]

set_property PACKAGE_PIN AN15 [get_ports {c0_ddr3_addr[0]}]
set_property PACKAGE_PIN AM15 [get_ports {c0_ddr3_addr[1]}]
set_property PACKAGE_PIN AG15 [get_ports {c0_ddr3_addr[10]}]
set_property PACKAGE_PIN AP17 [get_ports {c0_ddr3_addr[11]}]
set_property PACKAGE_PIN AL18 [get_ports {c0_ddr3_addr[12]}]
set_property PACKAGE_PIN AH19 [get_ports {c0_ddr3_addr[13]}]
set_property PACKAGE_PIN AP15 [get_ports {c0_ddr3_addr[14]}]
set_property PACKAGE_PIN AP14 [get_ports {c0_ddr3_addr[15]}]
set_property PACKAGE_PIN AN14 [get_ports {c0_ddr3_addr[2]}]
set_property PACKAGE_PIN AJ17 [get_ports {c0_ddr3_addr[3]}]
set_property PACKAGE_PIN AL16 [get_ports {c0_ddr3_addr[4]}]
set_property PACKAGE_PIN AK18 [get_ports {c0_ddr3_addr[5]}]
set_property PACKAGE_PIN AL15 [get_ports {c0_ddr3_addr[6]}]
set_property PACKAGE_PIN AK14 [get_ports {c0_ddr3_addr[7]}]
set_property PACKAGE_PIN AK17 [get_ports {c0_ddr3_addr[8]}]
set_property PACKAGE_PIN AP16 [get_ports {c0_ddr3_addr[9]}]
set_property PACKAGE_PIN AH14 [get_ports {c0_ddr3_ba[0]}]
set_property PACKAGE_PIN AL14 [get_ports {c0_ddr3_ba[1]}]
set_property PACKAGE_PIN AM18 [get_ports {c0_ddr3_ba[2]}]
set_property PACKAGE_PIN AF15 [get_ports c0_ddr3_cas_n]
# set_property PACKAGE_PIN AN20 [get_ports {c0_ddr3_cb[0]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN AP19 [get_ports {c0_ddr3_cb[1]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN AP20 [get_ports {c0_ddr3_cb[2]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN AP22 [get_ports {c0_ddr3_cb[3]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN AN19 [get_ports {c0_ddr3_cb[4]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN AP21 [get_ports {c0_ddr3_cb[5]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN AP24 [get_ports {c0_ddr3_cb[6]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN AN24 [get_ports {c0_ddr3_cb[7]}] # ECC CHECK BIT PIN
set_property PACKAGE_PIN AH17 [get_ports {c0_ddr3_ck_n[0]}]
set_property PACKAGE_PIN AG17 [get_ports {c0_ddr3_ck_p[0]}]
set_property PACKAGE_PIN AG16 [get_ports {c0_ddr3_ck_n[1]}]
set_property PACKAGE_PIN AF16 [get_ports {c0_ddr3_ck_p[1]}]
set_property PACKAGE_PIN AN18 [get_ports {c0_ddr3_cke[0]}]
set_property PACKAGE_PIN AM17 [get_ports {c0_ddr3_cke[1]}]
set_property PACKAGE_PIN AF18 [get_ports {c0_ddr3_cs_n[0]}]
set_property PACKAGE_PIN AG18 [get_ports {c0_ddr3_cs_n[1]}]
set_property PACKAGE_PIN AH10 [get_ports {c0_ddr3_dm[0]}]
set_property PACKAGE_PIN AF9  [get_ports {c0_ddr3_dm[1]}]
set_property PACKAGE_PIN AM13 [get_ports {c0_ddr3_dm[2]}]
set_property PACKAGE_PIN AL10 [get_ports {c0_ddr3_dm[3]}]
set_property PACKAGE_PIN AL20 [get_ports {c0_ddr3_dm[4]}]
set_property PACKAGE_PIN AJ24 [get_ports {c0_ddr3_dm[5]}]
set_property PACKAGE_PIN AD22 [get_ports {c0_ddr3_dm[6]}]
set_property PACKAGE_PIN AD15 [get_ports {c0_ddr3_dm[7]}]
# set_property PACKAGE_PIN AM23 [get_ports {c0_ddr3_dm[8]}] # ECC-USED PIN
set_property PACKAGE_PIN AF11 [get_ports {c0_ddr3_dq[0]}]
set_property PACKAGE_PIN AH9  [get_ports {c0_ddr3_dq[1]}]
set_property PACKAGE_PIN AE8  [get_ports {c0_ddr3_dq[10]}]
set_property PACKAGE_PIN AD9  [get_ports {c0_ddr3_dq[11]}]
set_property PACKAGE_PIN AE9  [get_ports {c0_ddr3_dq[12]}]
set_property PACKAGE_PIN AF8  [get_ports {c0_ddr3_dq[13]}]
set_property PACKAGE_PIN AD10 [get_ports {c0_ddr3_dq[14]}]
set_property PACKAGE_PIN AD12 [get_ports {c0_ddr3_dq[15]}]
set_property PACKAGE_PIN AN9  [get_ports {c0_ddr3_dq[16]}]
set_property PACKAGE_PIN AP9  [get_ports {c0_ddr3_dq[17]}]
set_property PACKAGE_PIN AP11 [get_ports {c0_ddr3_dq[18]}]
set_property PACKAGE_PIN AP12 [get_ports {c0_ddr3_dq[19]}]
set_property PACKAGE_PIN AH8  [get_ports {c0_ddr3_dq[2]}]
set_property PACKAGE_PIN AM12 [get_ports {c0_ddr3_dq[20]}]
set_property PACKAGE_PIN AN13 [get_ports {c0_ddr3_dq[21]}]
set_property PACKAGE_PIN AK13 [get_ports {c0_ddr3_dq[22]}]
set_property PACKAGE_PIN AL13 [get_ports {c0_ddr3_dq[23]}]
set_property PACKAGE_PIN AK9  [get_ports {c0_ddr3_dq[24]}]
set_property PACKAGE_PIN AL9  [get_ports {c0_ddr3_dq[25]}]
set_property PACKAGE_PIN AM10 [get_ports {c0_ddr3_dq[26]}]
set_property PACKAGE_PIN AK12 [get_ports {c0_ddr3_dq[27]}]
set_property PACKAGE_PIN AL11 [get_ports {c0_ddr3_dq[28]}]
set_property PACKAGE_PIN AK11 [get_ports {c0_ddr3_dq[29]}]
set_property PACKAGE_PIN AJ10 [get_ports {c0_ddr3_dq[3]}]
set_property PACKAGE_PIN AJ12 [get_ports {c0_ddr3_dq[30]}]
set_property PACKAGE_PIN AJ11 [get_ports {c0_ddr3_dq[31]}]
set_property PACKAGE_PIN AL23 [get_ports {c0_ddr3_dq[32]}]
set_property PACKAGE_PIN AK23 [get_ports {c0_ddr3_dq[33]}]
set_property PACKAGE_PIN AL24 [get_ports {c0_ddr3_dq[34]}]
set_property PACKAGE_PIN AK24 [get_ports {c0_ddr3_dq[35]}]
set_property PACKAGE_PIN AJ22 [get_ports {c0_ddr3_dq[36]}]
set_property PACKAGE_PIN AL21 [get_ports {c0_ddr3_dq[37]}]
set_property PACKAGE_PIN AM20 [get_ports {c0_ddr3_dq[38]}]
set_property PACKAGE_PIN AM21 [get_ports {c0_ddr3_dq[39]}]
set_property PACKAGE_PIN AF10 [get_ports {c0_ddr3_dq[4]}]
set_property PACKAGE_PIN AJ21 [get_ports {c0_ddr3_dq[40]}]
set_property PACKAGE_PIN AJ20 [get_ports {c0_ddr3_dq[41]}]
set_property PACKAGE_PIN AG22 [get_ports {c0_ddr3_dq[42]}]
set_property PACKAGE_PIN AG20 [get_ports {c0_ddr3_dq[43]}]
set_property PACKAGE_PIN AJ19 [get_ports {c0_ddr3_dq[44]}]
set_property PACKAGE_PIN AK21 [get_ports {c0_ddr3_dq[45]}]
set_property PACKAGE_PIN AH23 [get_ports {c0_ddr3_dq[46]}]
set_property PACKAGE_PIN AH22 [get_ports {c0_ddr3_dq[47]}]
set_property PACKAGE_PIN AE19 [get_ports {c0_ddr3_dq[48]}]
set_property PACKAGE_PIN AG21 [get_ports {c0_ddr3_dq[49]}]
set_property PACKAGE_PIN AG8  [get_ports {c0_ddr3_dq[5]}]
set_property PACKAGE_PIN AE22 [get_ports {c0_ddr3_dq[50]}]
set_property PACKAGE_PIN AF21 [get_ports {c0_ddr3_dq[51]}]
set_property PACKAGE_PIN AD19 [get_ports {c0_ddr3_dq[52]}]
set_property PACKAGE_PIN AE21 [get_ports {c0_ddr3_dq[53]}]
set_property PACKAGE_PIN AC19 [get_ports {c0_ddr3_dq[54]}]
set_property PACKAGE_PIN AD21 [get_ports {c0_ddr3_dq[55]}]
set_property PACKAGE_PIN AF13 [get_ports {c0_ddr3_dq[56]}]
set_property PACKAGE_PIN AE16 [get_ports {c0_ddr3_dq[57]}]
set_property PACKAGE_PIN AD16 [get_ports {c0_ddr3_dq[58]}]
set_property PACKAGE_PIN AD17 [get_ports {c0_ddr3_dq[59]}]
set_property PACKAGE_PIN AG10 [get_ports {c0_ddr3_dq[6]}]
set_property PACKAGE_PIN AE13 [get_ports {c0_ddr3_dq[60]}]
set_property PACKAGE_PIN AD14 [get_ports {c0_ddr3_dq[61]}]
set_property PACKAGE_PIN AC17 [get_ports {c0_ddr3_dq[62]}]
set_property PACKAGE_PIN AE17 [get_ports {c0_ddr3_dq[63]}]
set_property PACKAGE_PIN AG11 [get_ports {c0_ddr3_dq[7]}]
set_property PACKAGE_PIN AC10 [get_ports {c0_ddr3_dq[8]}]
set_property PACKAGE_PIN AC9  [get_ports {c0_ddr3_dq[9]}]
set_property PACKAGE_PIN AH12 [get_ports {c0_ddr3_dqs_n[0]}]
set_property PACKAGE_PIN AG12 [get_ports {c0_ddr3_dqs_p[0]}]
set_property PACKAGE_PIN AE11 [get_ports {c0_ddr3_dqs_n[1]}]
set_property PACKAGE_PIN AE12 [get_ports {c0_ddr3_dqs_p[1]}]
set_property PACKAGE_PIN AP10 [get_ports {c0_ddr3_dqs_n[2]}]
set_property PACKAGE_PIN AN10 [get_ports {c0_ddr3_dqs_p[2]}]
set_property PACKAGE_PIN AL8  [get_ports {c0_ddr3_dqs_n[3]}]
set_property PACKAGE_PIN AK8  [get_ports {c0_ddr3_dqs_p[3]}]
set_property PACKAGE_PIN AL19 [get_ports {c0_ddr3_dqs_n[4]}]
set_property PACKAGE_PIN AK19 [get_ports {c0_ddr3_dqs_p[4]}]
set_property PACKAGE_PIN AG23 [get_ports {c0_ddr3_dqs_n[5]}]
set_property PACKAGE_PIN AF23 [get_ports {c0_ddr3_dqs_p[5]}]
set_property PACKAGE_PIN AF20 [get_ports {c0_ddr3_dqs_n[6]}]
set_property PACKAGE_PIN AF19 [get_ports {c0_ddr3_dqs_p[6]}]
set_property PACKAGE_PIN AF14 [get_ports {c0_ddr3_dqs_n[7]}]
set_property PACKAGE_PIN AE14 [get_ports {c0_ddr3_dqs_p[7]}]
# set_property PACKAGE_PIN AN22 [get_ports {c0_ddr3_dqs_n[8]}] # ECC-USED PIN
# set_property PACKAGE_PIN AM22 [get_ports {c0_ddr3_dqs_p[8]}] # ECC-USED PIN
# set_property PACKAGE_PIN AB28 [get_ports c0_ddr3_event_n] # TERMPERATURE EVENT PIN
set_property PACKAGE_PIN AK16 [get_ports {c0_ddr3_odt[0]}]
set_property PACKAGE_PIN AM16 [get_ports {c0_ddr3_odt[1]}]
set_property PACKAGE_PIN AH18 [get_ports c0_ddr3_ras_n]
set_property PACKAGE_PIN AJ14 [get_ports c0_ddr3_reset_n]
set_property PACKAGE_PIN AJ16 [get_ports c0_ddr3_we_n]
set_property PACKAGE_PIN K27 [get_ports {c1_ddr3_addr[0]}]
set_property PACKAGE_PIN L26 [get_ports {c1_ddr3_addr[1]}]
set_property PACKAGE_PIN K29 [get_ports {c1_ddr3_addr[10]}]
set_property PACKAGE_PIN L29 [get_ports {c1_ddr3_addr[11]}]
set_property PACKAGE_PIN E34 [get_ports {c1_ddr3_addr[12]}]
set_property PACKAGE_PIN K24 [get_ports {c1_ddr3_addr[13]}]
set_property PACKAGE_PIN H32 [get_ports {c1_ddr3_addr[14]}]
set_property PACKAGE_PIN F31 [get_ports {c1_ddr3_addr[15]}]
set_property PACKAGE_PIN L23 [get_ports {c1_ddr3_addr[2]}]
set_property PACKAGE_PIN G33 [get_ports {c1_ddr3_addr[3]}]
set_property PACKAGE_PIN M26 [get_ports {c1_ddr3_addr[4]}]
set_property PACKAGE_PIN E32 [get_ports {c1_ddr3_addr[5]}]
set_property PACKAGE_PIN M27 [get_ports {c1_ddr3_addr[6]}]
set_property PACKAGE_PIN M25 [get_ports {c1_ddr3_addr[7]}]
set_property PACKAGE_PIN F30 [get_ports {c1_ddr3_addr[8]}]
set_property PACKAGE_PIN J29 [get_ports {c1_ddr3_addr[9]}]
set_property PACKAGE_PIN K28 [get_ports {c1_ddr3_ba[0]}]
set_property PACKAGE_PIN K23 [get_ports {c1_ddr3_ba[1]}]
set_property PACKAGE_PIN G32 [get_ports {c1_ddr3_ba[2]}]
set_property PACKAGE_PIN F29 [get_ports c1_ddr3_cas_n]
# set_property PACKAGE_PIN N28 [get_ports {c1_ddr3_cb[0]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN N27 [get_ports {c1_ddr3_cb[1]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN N24 [get_ports {c1_ddr3_cb[2]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN P26 [get_ports {c1_ddr3_cb[3]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN N25 [get_ports {c1_ddr3_cb[4]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN P24 [get_ports {c1_ddr3_cb[5]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN T24 [get_ports {c1_ddr3_cb[6]}] # ECC CHECK BIT PIN
# set_property PACKAGE_PIN T25 [get_ports {c1_ddr3_cb[7]}] # ECC CHECK BIT PIN
set_property PACKAGE_PIN H30 [get_ports {c1_ddr3_ck_n[0]}]
set_property PACKAGE_PIN H29 [get_ports {c1_ddr3_ck_p[0]}]
set_property PACKAGE_PIN J31 [get_ports {c1_ddr3_ck_n[1]}]
set_property PACKAGE_PIN J30 [get_ports {c1_ddr3_ck_p[1]}]
set_property PACKAGE_PIN F33 [get_ports {c1_ddr3_cke[0]}]
set_property PACKAGE_PIN F34 [get_ports {c1_ddr3_cke[1]}]
set_property PACKAGE_PIN H28 [get_ports {c1_ddr3_cs_n[0]}]
set_property PACKAGE_PIN G28 [get_ports {c1_ddr3_cs_n[1]}]
set_property PACKAGE_PIN B32 [get_ports {c1_ddr3_dm[0]}]
set_property PACKAGE_PIN A30 [get_ports {c1_ddr3_dm[1]}]
set_property PACKAGE_PIN E24 [get_ports {c1_ddr3_dm[2]}]
set_property PACKAGE_PIN B26 [get_ports {c1_ddr3_dm[3]}]
set_property PACKAGE_PIN U31 [get_ports {c1_ddr3_dm[4]}]
set_property PACKAGE_PIN R29 [get_ports {c1_ddr3_dm[5]}]
set_property PACKAGE_PIN K34 [get_ports {c1_ddr3_dm[6]}]
set_property PACKAGE_PIN N34 [get_ports {c1_ddr3_dm[7]}]
# set_property PACKAGE_PIN P25 [get_ports {c1_ddr3_dm[8]}] # ECC-USED PIN
set_property PACKAGE_PIN C33 [get_ports {c1_ddr3_dq[0]}]
set_property PACKAGE_PIN D34 [get_ports {c1_ddr3_dq[1]}]
set_property PACKAGE_PIN A31 [get_ports {c1_ddr3_dq[10]}]
set_property PACKAGE_PIN C30 [get_ports {c1_ddr3_dq[11]}]
set_property PACKAGE_PIN B30 [get_ports {c1_ddr3_dq[12]}]
set_property PACKAGE_PIN C29 [get_ports {c1_ddr3_dq[13]}]
set_property PACKAGE_PIN E28 [get_ports {c1_ddr3_dq[14]}]
set_property PACKAGE_PIN C28 [get_ports {c1_ddr3_dq[15]}]
set_property PACKAGE_PIN G27 [get_ports {c1_ddr3_dq[16]}]
set_property PACKAGE_PIN F28 [get_ports {c1_ddr3_dq[17]}]
set_property PACKAGE_PIN F26 [get_ports {c1_ddr3_dq[18]}]
set_property PACKAGE_PIN J24 [get_ports {c1_ddr3_dq[19]}]
set_property PACKAGE_PIN D32 [get_ports {c1_ddr3_dq[2]}]
set_property PACKAGE_PIN D25 [get_ports {c1_ddr3_dq[20]}]
set_property PACKAGE_PIN G26 [get_ports {c1_ddr3_dq[21]}]
set_property PACKAGE_PIN H25 [get_ports {c1_ddr3_dq[22]}]
set_property PACKAGE_PIN J25 [get_ports {c1_ddr3_dq[23]}]
set_property PACKAGE_PIN A25 [get_ports {c1_ddr3_dq[24]}]
set_property PACKAGE_PIN A26 [get_ports {c1_ddr3_dq[25]}]
set_property PACKAGE_PIN B27 [get_ports {c1_ddr3_dq[26]}]
set_property PACKAGE_PIN C27 [get_ports {c1_ddr3_dq[27]}]
set_property PACKAGE_PIN B25 [get_ports {c1_ddr3_dq[28]}]
set_property PACKAGE_PIN C25 [get_ports {c1_ddr3_dq[29]}]
set_property PACKAGE_PIN B31 [get_ports {c1_ddr3_dq[3]}]
set_property PACKAGE_PIN D27 [get_ports {c1_ddr3_dq[30]}]
set_property PACKAGE_PIN E27 [get_ports {c1_ddr3_dq[31]}]
set_property PACKAGE_PIN P29 [get_ports {c1_ddr3_dq[32]}]
set_property PACKAGE_PIN M30 [get_ports {c1_ddr3_dq[33]}]
set_property PACKAGE_PIN U32 [get_ports {c1_ddr3_dq[34]}]
set_property PACKAGE_PIN M31 [get_ports {c1_ddr3_dq[35]}]
set_property PACKAGE_PIN N29 [get_ports {c1_ddr3_dq[36]}]
set_property PACKAGE_PIN N30 [get_ports {c1_ddr3_dq[37]}]
set_property PACKAGE_PIN R31 [get_ports {c1_ddr3_dq[38]}]
set_property PACKAGE_PIN P31 [get_ports {c1_ddr3_dq[39]}]
set_property PACKAGE_PIN C34 [get_ports {c1_ddr3_dq[4]}]
set_property PACKAGE_PIN U26 [get_ports {c1_ddr3_dq[40]}]
set_property PACKAGE_PIN U27 [get_ports {c1_ddr3_dq[41]}]
set_property PACKAGE_PIN R28 [get_ports {c1_ddr3_dq[42]}]
set_property PACKAGE_PIN T30 [get_ports {c1_ddr3_dq[43]}]
set_property PACKAGE_PIN T28 [get_ports {c1_ddr3_dq[44]}]
set_property PACKAGE_PIN U25 [get_ports {c1_ddr3_dq[45]}]
set_property PACKAGE_PIN T29 [get_ports {c1_ddr3_dq[46]}]
set_property PACKAGE_PIN U28 [get_ports {c1_ddr3_dq[47]}]
set_property PACKAGE_PIN L31 [get_ports {c1_ddr3_dq[48]}]
set_property PACKAGE_PIN L33 [get_ports {c1_ddr3_dq[49]}]
set_property PACKAGE_PIN B34 [get_ports {c1_ddr3_dq[5]}]
set_property PACKAGE_PIN K32 [get_ports {c1_ddr3_dq[50]}]
set_property PACKAGE_PIN K31 [get_ports {c1_ddr3_dq[51]}]
set_property PACKAGE_PIN L34 [get_ports {c1_ddr3_dq[52]}]
set_property PACKAGE_PIN L30 [get_ports {c1_ddr3_dq[53]}]
set_property PACKAGE_PIN J34 [get_ports {c1_ddr3_dq[54]}]
set_property PACKAGE_PIN K33 [get_ports {c1_ddr3_dq[55]}]
set_property PACKAGE_PIN T34 [get_ports {c1_ddr3_dq[56]}]
set_property PACKAGE_PIN R33 [get_ports {c1_ddr3_dq[57]}]
set_property PACKAGE_PIN M32 [get_ports {c1_ddr3_dq[58]}]
set_property PACKAGE_PIN M33 [get_ports {c1_ddr3_dq[59]}]
set_property PACKAGE_PIN D31 [get_ports {c1_ddr3_dq[6]}]
set_property PACKAGE_PIN U33 [get_ports {c1_ddr3_dq[60]}]
set_property PACKAGE_PIN P32 [get_ports {c1_ddr3_dq[61]}]
set_property PACKAGE_PIN P34 [get_ports {c1_ddr3_dq[62]}]
set_property PACKAGE_PIN N33 [get_ports {c1_ddr3_dq[63]}]
set_property PACKAGE_PIN E31 [get_ports {c1_ddr3_dq[7]}]
set_property PACKAGE_PIN D30 [get_ports {c1_ddr3_dq[8]}]
set_property PACKAGE_PIN D29 [get_ports {c1_ddr3_dq[9]}]
set_property PACKAGE_PIN A33 [get_ports {c1_ddr3_dqs_n[0]}]
set_property PACKAGE_PIN B33 [get_ports {c1_ddr3_dqs_p[0]}]
set_property PACKAGE_PIN A29 [get_ports {c1_ddr3_dqs_n[1]}]
set_property PACKAGE_PIN A28 [get_ports {c1_ddr3_dqs_p[1]}]
set_property PACKAGE_PIN F25 [get_ports {c1_ddr3_dqs_n[2]}]
set_property PACKAGE_PIN G25 [get_ports {c1_ddr3_dqs_p[2]}]
set_property PACKAGE_PIN D26 [get_ports {c1_ddr3_dqs_n[3]}]
set_property PACKAGE_PIN E26 [get_ports {c1_ddr3_dqs_p[3]}]
set_property PACKAGE_PIN R32 [get_ports {c1_ddr3_dqs_n[4]}]
set_property PACKAGE_PIN T31 [get_ports {c1_ddr3_dqs_p[4]}]
set_property PACKAGE_PIN R27 [get_ports {c1_ddr3_dqs_n[5]}]
set_property PACKAGE_PIN R26 [get_ports {c1_ddr3_dqs_p[5]}]
set_property PACKAGE_PIN H34 [get_ports {c1_ddr3_dqs_n[6]}]
set_property PACKAGE_PIN H33 [get_ports {c1_ddr3_dqs_p[6]}]
set_property PACKAGE_PIN R34 [get_ports {c1_ddr3_dqs_n[7]}]
set_property PACKAGE_PIN T33 [get_ports {c1_ddr3_dqs_p[7]}]
# set_property PACKAGE_PIN R24 [get_ports {c1_ddr3_dqs_n[8]}] # ECC-USED PIN
# set_property PACKAGE_PIN R23 [get_ports {c1_ddr3_dqs_p[8]}] # ECC-USED PIN
# set_property PACKAGE_PIN Y26 [get_ports c1_ddr3_event_n] # TERMPERATURE EVENT PIN
set_property PACKAGE_PIN K26 [get_ports {c1_ddr3_odt[0]}]
set_property PACKAGE_PIN L24 [get_ports {c1_ddr3_odt[1]}]
set_property PACKAGE_PIN J27 [get_ports c1_ddr3_ras_n]
set_property PACKAGE_PIN E33 [get_ports c1_ddr3_reset_n]
set_property PACKAGE_PIN L28 [get_ports c1_ddr3_we_n]