#
# adm_pcie_x8.xdc
#
# Short Description: The Xilinx Design Constraints files that connects FPGA pins
#  with the PCI Express endpoint encapsulation module and creates related clocks
#  for synthesis.
#
# Long Description: The Xilinx Design Constraints files that connects FPGA pins
#  with the PCI Express endpoint encapsulation module and creates related clocks
#  for synthesis.
#
# Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

set_property PACKAGE_PIN F6 [get_ports pcie100_p]
# set_property IOSTANDARD LVDS [get_ports pcie100_p]
# [Vivado 12-1815] Setting property 'IOSTANDARD' is not allowed for GT terminals.
create_clock -period 10.0 [get_ports pcie100_p]
set_input_jitter pcie100_p 0.050

set_property PACKAGE_PIN F5 [get_ports pcie100_n]

set_property PACKAGE_PIN W27 [get_ports perst_n]
set_property IOSTANDARD LVCMOS18 [get_ports perst_n]

set_property PACKAGE_PIN A4 [get_ports pci_exp_txp[0]]
set_property PACKAGE_PIN A3 [get_ports pci_exp_txn[0]]
set_property PACKAGE_PIN B6 [get_ports pci_exp_rxp[0]]
set_property PACKAGE_PIN B5 [get_ports pci_exp_rxn[0]]

set_property PACKAGE_PIN B2 [get_ports pci_exp_txp[1]]
set_property PACKAGE_PIN B1 [get_ports pci_exp_txn[1]]
set_property PACKAGE_PIN D6 [get_ports pci_exp_rxp[1]]
set_property PACKAGE_PIN D5 [get_ports pci_exp_rxn[1]]

set_property PACKAGE_PIN C4 [get_ports pci_exp_txp[2]]
set_property PACKAGE_PIN C3 [get_ports pci_exp_txn[2]]
set_property PACKAGE_PIN E4 [get_ports pci_exp_rxp[2]]
set_property PACKAGE_PIN E3 [get_ports pci_exp_rxn[2]]

set_property PACKAGE_PIN D2 [get_ports pci_exp_txp[3]]
set_property PACKAGE_PIN D1 [get_ports pci_exp_txn[3]]
set_property PACKAGE_PIN G4 [get_ports pci_exp_rxp[3]]
set_property PACKAGE_PIN G3 [get_ports pci_exp_rxn[3]]

set_property PACKAGE_PIN F2 [get_ports pci_exp_txp[4]]
set_property PACKAGE_PIN F1 [get_ports pci_exp_txn[4]]
set_property PACKAGE_PIN J4 [get_ports pci_exp_rxp[4]]
set_property PACKAGE_PIN J3 [get_ports pci_exp_rxn[4]]

set_property PACKAGE_PIN H2 [get_ports pci_exp_txp[5]]
set_property PACKAGE_PIN H1 [get_ports pci_exp_txn[5]]
set_property PACKAGE_PIN K6 [get_ports pci_exp_rxp[5]]
set_property PACKAGE_PIN K5 [get_ports pci_exp_rxn[5]]

set_property PACKAGE_PIN K2 [get_ports pci_exp_txp[6]]
set_property PACKAGE_PIN K1 [get_ports pci_exp_txn[6]]
set_property PACKAGE_PIN L4 [get_ports pci_exp_rxp[6]]
set_property PACKAGE_PIN L3 [get_ports pci_exp_rxn[6]]

set_property PACKAGE_PIN M2 [get_ports pci_exp_txp[7]]
set_property PACKAGE_PIN M1 [get_ports pci_exp_txn[7]]
set_property PACKAGE_PIN N4 [get_ports pci_exp_rxp[7]]
set_property PACKAGE_PIN N3 [get_ports pci_exp_rxn[7]]
