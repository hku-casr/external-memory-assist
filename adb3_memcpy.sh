#!/bin/bash

#
# Name: adb3_memcpy.sh
#
# Description: Initialise DDR3 memory of attached Alpha-Data ADM-PCIE-7V3 FPGA
#  through PCI Express by ADB3 API.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

usage="Initialise DDR3 memory of attached Alpha-Data ADM-PCIE-7V3 FPGA
Usage: $0 <binary_file> [<fpga_index> [<mem_ctrl_id>]]
 e.g.: $0 dump.bin 0 1";

# PLATFORM SPECIFIC PARAMETERS
#
# PAGE_SIZE: STANDARD PCI EXPRESS BASE ADDRESS REGISTER WINDOW = 4KB
# MC_SIZE: ADDRESSABLE RANGE OF SINGLE MEMORY CONTROLLER ON ADMPCIE7V3,
#           I.E., INSTALLED MEMORY SIZE PER DIMM SLOT ON ADMPCIE7V3 = 8GB
#
PAGE_SIZE=4096;
MC_SIZE=0x200000000;

case $# in
    3)
        (printf "%d" $2 && printf "%d $3") > /dev/null 2> /dev/null;

        if [ $? -ne 0 ]; then
            echo "$usage";

            exit 1;
        else
            card_id=$2;

            mc_id=$3;
        fi;

        ;;
    2)
        printf "%d" $2 > /dev/null 2> /dev/null;

        if [ $? -ne 0 ]; then
            echo "$usage";

            exit 1;
        else
            card_id=$2;

            mc_id=0;
        fi;

        ;;
    1)
        card_id=0;

        mc_id=0;

        ;;
    *)
        echo "$usage";

        exit 1;

        ;;
esac;

mcspcaddr=0x0000;

# FLIP MEMORY PAGE TO NON-0 MEMORY CONTROLLER ADDRESS RANGE IF NEEDED
#
if [ $mc_id -ne 0 ]; then
    printf "Flip memory page to 0x%016x..." $((mcspcaddr+MC_SIZE*mc_id));

    echo -ne "$((mcspcaddr+MC_SIZE*mc_id))" | sudo /opt/Alpha-Data/apps/linux/dump/dump -index $card_id -hex -be wq 3 0x20 8 > /dev/null;

    echo " Completed."
fi;

# ESTIMATE BINARY SIZE
#
binbound=$(cat $1 | wc -c);

# START OF ACTUAL MEMCPY ROUTINE
#
printf "memcpy into 0x%016x...\n" $((mcspcaddr+MC_SIZE*mc_id));

tmpf=`mktemp`;

dd if=$1 ibs=1 skip=$mcspcaddr count=$PAGE_SIZE of=$tmpf status=none;
cat $tmpf | od -w4 -vt x1 | cut -d " " -f2- | sed "s/ //g" | sudo /opt/Alpha-Data/apps/linux/dump/dump -index $card_id +hex +be wd 0 0 $PAGE_SIZE > /dev/null;

rm $tmpf;

mcspcaddr=$((mcspcaddr+PAGE_SIZE));

while [ $mcspcaddr -lt $binbound ]; do
    # FLIP MEMORY PAGE AFTER LEAPING THE 4KB BASE ADDRESS REGISTER ADDRESSABLE RANGE
    #
    printf "Flip memory page to 0x%016x..." $((mcspcaddr+MC_SIZE*mc_id));

    echo -ne "$((mcspcaddr+MC_SIZE*mc_id))" | sudo /opt/Alpha-Data/apps/linux/dump/dump -index $card_id -hex -be wq 3 0x20 8 > /dev/null;

    echo -ne " memcpy...";

    tmpf=`mktemp`;

    dd if=$1 ibs=1 skip=$mcspcaddr count=$PAGE_SIZE of=$tmpf status=none;
    cat $tmpf | od -w4 -vt x1 | cut -d " " -f2- | sed "s/ //g" | sudo /opt/Alpha-Data/apps/linux/dump/dump -index $card_id +hex +be wd 0 0 $PAGE_SIZE > /dev/null;

    rm $tmpf;

    echo " Completed."

    mcspcaddr=$((mcspcaddr+PAGE_SIZE));
done;

exit 0;