#!/bin/make

#
# Name: Makefile
#
# Description: This Makefile constructs a Vivado project, instantiates and
#  populates individual filesets for conducting implementation and simulations.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

#
# PROJECT PROPERTIES
#
PROJ_NAME = xmem_assist
PROJ_XPR = ./proj/$(PROJ_NAME).xpr

#
# PRE-DEFINED FILE GROUPS
#
GENERIC_BASE = ../common-verilog-utilities
GENERIC_FILE = miscellaneous.v
VENDOR_BASE  = .
VENDOR_FILE  = adm_pcie_x8_2c_ddr3_x64_axi4_ipi_top.v       \
                adm_pcie_x8_2c_ddr3_x64_axi4_ipi_prep.tcl   \
                adm_pcie_x8_2c_ddr3_x64_axi4_ipi_bd.tcl
VENDOR_CSTRS = adm_pcie_x8.xdc                              \
                adm_2c_ddr3_x64.xdc                         \
                adm_pcie_2c_ddr3_cdc.xdc                    \
                vio_master.xdc

#
# USER-DEFINED FILESETS
#
SRC_SET = sources_1
SIM_SET = sim_1
CONSTRS_SET = constrs_1

sources_1_FSET  = $(addprefix $(VENDOR_BASE)/, $(VENDOR_FILE))
sources_1_FSET += $(addprefix $(GENERIC_BASE)/, $(GENERIC_FILE))
constrs_1_FSET  = $(addprefix $(VENDOR_BASE)/, $(VENDOR_CSTRS))

#
# MAKEFILE LOGICS, DO NOT TOUCH!!!
#
VV = /opt/Xilinx/Vivado/2015.4/bin/vivado
VVFLAG = -mode tcl

project: %: %.p $(SRC_SET) $(SIM_SET) $(CONSTRS_SET)
	@echo "CREATED PROJECT $@"

project.p:
	@$(VV) $(VVFLAG) <<< "create_project -part xc7vx690tffg1157-2 $(PROJ_NAME) ./proj"

$(SRC_SET) $(SIM_SET) $(CONSTRS_SET): %: %.p %.s
	@echo "CREATED FILESET $@"

$(addsuffix .p, $(SRC_SET)):
	@$(VV) $(VVFLAG) $(PROJ_XPR) <<< "create_fileset $(@:.p=)"

$(addsuffix .p, $(SIM_SET)):
	@$(VV) $(VVFLAG) $(PROJ_XPR) <<< "create_fileset -simset $(@:.p=)"

	# EXCLUDE FILES IN SIMSET UNLESS SIMSET IS PRECEEDED BY NAME "SIM_"
	#
	@$(foreach fset, $(filter-out sim_%, $(@:.p=)),  $(VV) $(VVFLAG) $(PROJ_XPR) <<< "set_property SOURCE_SET {} [get_filesets $(@:.p=)]")

$(addsuffix .p, $(CONSTRS_SET)):
	@$(VV) $(VVFLAG) $(PROJ_XPR) <<< "create_fileset -constrset $(@:.p=)"

.SECONDEXPANSION:
$(addsuffix .s, $(SRC_SET)) $(addsuffix .s, $(SIM_SET)) $(addsuffix .s, $(CONSTRS_SET)): %.s: %.p $$(%_FSET)
	@$(VV) $(VVFLAG) $(PROJ_XPR) <<< "add_files -fileset $(@:.s=) $(filter-out $<, $^)"

	# IF FILE TYPE IS A TCL SCRIPT FILE, EXECUTE IT
	#
	@$(foreach tcl_f, $(filter %.tcl, $(filter-out $<, $^)), $(VV) $(VVFLAG) $(PROJ_XPR) <<< "set_property IS_ENABLED 0 [get_files $(tcl_f)]; source {$(tcl_f)}";)
