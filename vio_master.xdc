#
# vio_master.xdc
#
# Short Description: The Xilinx Design Constraints file that disables certain
#  nets of asynchronous nature from timing analysis.
#
# Long Description: The Xilinx Design Constraints file that disables certain
#  nets of asynchronous nature from timing analysis.
#
# Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

set_false_path -through [get_nets ctrl_panel/inst/probe_*]